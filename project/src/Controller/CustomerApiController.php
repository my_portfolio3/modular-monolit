<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\CustomerService;
use App\Dto\CreateOrderRequest;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/customer', defaults: ['_format' => 'json'])]
final class CustomerApiController extends AbstractController
{
    #[Route('/order', methods: ['POST'])]
    public function createOrder(
        #[MapRequestPayload] CreateOrderRequest $request,
        CustomerService $customerService
    ): Response {
        try {
            $orderId = $customerService->createOrder($request);
        } catch (\Throwable $th) {
            return $this->json(['message' => 'Restaurant not found'], Response::HTTP_BAD_REQUEST);
        };

        return $this->json(['id' => $orderId]);
    }
}
