<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Delivery;
use App\Entity\Order;
use App\Exception\EntityNotFoundException;
use App\Repository\DeliveryRepository;
use Doctrine\ORM\EntityManagerInterface;

readonly final class CourierService
{
    public function __construct(private EntityManagerInterface $em, private DeliveryRepository $repo)
    {
    }

    public function createDelivery(Order $order): ?Delivery
    {
        $delivery = (new Delivery())
            ->setStatus(Delivery::STATUS_NEW)
            ->setRelatedOrder($order);

        $this->em->persist($delivery);
        $this->em->flush();

        return $delivery;
    }

    public function changeDeliveryStatus(int $deliveryId, string $status): Delivery
    {
        $delivery = $this->repo->find($deliveryId);
        
        if (!$delivery || !$delivery->getRelatedOrder()?->getId()) {
            throw new EntityNotFoundException();
        }
        
        $delivery->setStatus($status);
        
        $this->em->persist($delivery);
        $this->em->flush();

        return $delivery;
    }
}
