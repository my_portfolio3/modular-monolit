<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\CreateOrderRequest;
use App\Entity\Order;
use App\Exception\EntityNotFoundException;
use Doctrine\ORM\EntityManagerInterface;

readonly final class CustomerService
{
    public function __construct(
        private RestaurantService $restaurantService,
        private CourierService $deliveryService,
        private EntityManagerInterface $em
    ) {
    }

    public function createOrder(CreateOrderRequest $createOrderRequest): int
    {
        if (!($restaurant = $this->restaurantService->getRestaurant($createOrderRequest->getRestaurantId()))) {
            throw new EntityNotFoundException();
        }

        $order = (new Order())
            ->setStatus(Order::STATUS_NEW)
            ->setRestaurant($restaurant);
        
        if ($this->restaurantService->acceptOrder($order)) {
            $order->setStatus(Order::STATUS_ACCEPTED);
            $delivery = $this->deliveryService->createDelivery($order);
            $order->setDelivery($delivery);
        } else {
            $order->setStatus(Order::STATUS_DECLINED);
        }

        $this->em->persist($order);
        $this->em->flush();

        return $order->getId();
    }

    public function changeOrderStatus(int $orderId, string $status): void
    {
        if (!$orderId) {
            throw new EntityNotFoundException();
        }

        $order = $this->em->find(Order::class, $orderId);
        if (!$order) {
            throw new EntityNotFoundException();
        }

        $order->setStatus($status);

        $this->em->persist($order);
        $this->em->flush();
    }
}
