<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;
use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;

readonly final class RestaurantService
{
    public function __construct(private RestaurantRepository $repo)
    {
    }

    public function getRestaurant(int $restaurantId): ?Restaurant
    {
        return $this->repo->find($restaurantId);
    }

    public function acceptOrder(Order $order): bool
    {
        return true;
    }
}
